<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// K
	'signature_description' => 'Crée une signature email en format HTML à télécharger',
	'signature_nom' => 'Signature',
	'signature_slogan' => 'Signature email pour les auteurs',
);

?>